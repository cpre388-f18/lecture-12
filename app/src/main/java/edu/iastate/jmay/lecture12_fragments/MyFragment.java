package edu.iastate.jmay.lecture12_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyFragment extends Fragment {


    public MyFragment() {
        // Required empty public constructor
    }

    public void onButtonClick(View v) {
        Toast.makeText(getContext(), "From the fragment", Toast.LENGTH_SHORT).show();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my, container, false);

        Button myButton = v.findViewById(R.id.button2);
//        myButton.setOnClickListener(new handler());
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClick(v);
            }
        });

        return v;
    }

//    private class handler implements View.OnClickListener {
//        @Override
//        public void onClick(View v) {
//            onButtonClick(v);
//        }
//    }
}
