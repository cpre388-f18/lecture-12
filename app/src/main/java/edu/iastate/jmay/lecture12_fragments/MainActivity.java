package edu.iastate.jmay.lecture12_fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButton1Click(View v) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        Fragment mf = new MyFragment();
        transaction.replace(R.id.fragmentContainer, mf);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    public void onButton2Click(View v) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        Fragment mf = new SecondFragment();
        transaction.replace(R.id.fragmentContainer, mf);
        transaction.addToBackStack(null);

        transaction.commit();
    }
}
